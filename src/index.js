import $ from "jquery";
import { debounce } from "debounce";

import bugImage from "./bug.png";
import bgImage from "./bg.png";

import Point2d from "./Point2d";
import Vector2d from "./vector2d";

const APP_WIDTH = 1000;
const APP_HEIGHT = 600;
const BUG_INDENT = 26;

var app = new PIXI.Application(APP_WIDTH, APP_HEIGHT, {backgroundColor : 0x1099bb});
document.getElementById("app").appendChild(app.view);

var texture = PIXI.Texture.fromImage(bugImage);  
var bug = new PIXI.Sprite(texture);
bug.anchor.x = 0.5;  
bug.anchor.y = 0.5;
bug.position.x = 400;  
bug.position.y = 200;

var textureBg = PIXI.Texture.fromImage(bgImage);  
var bg = new PIXI.Sprite(textureBg);

app.stage.addChild(bg);
app.stage.addChild(bug);

//Editable params...
let speedDefaultCoeff = 2;
let isEscapeMode = false;
let isLinearMode = false;

$(document).ready(() => {

    $("#linearMode").change(function (){
      if($(this).is(':checked') ){
        isLinearMode = true;
      }
      else{
        isLinearMode = false;
      }
    });

    $("input[name=moveMode]").change(function (){
        const mode = $(this).val();
        if(mode === "escapeMode"){
            isEscapeMode = true;
        }
        else{
            isEscapeMode = false;
        }
    });

    function onSpeedValueChange () { 
        const speed = $("#speedValue").val();
        if($.isNumeric(speed) && speed > 0){
            speedDefaultCoeff = speed;
        }
        else{
            alert("Скорость может быть только положительными числом");
        }
    };

    $("#speedValue").keyup(debounce(onSpeedValueChange, 100)); 

});
//...Editable params


function rotateBug(cursorPoint, bugPoint){  
    const dist_X = cursorPoint.x - bugPoint.x;
    const dist_Y = cursorPoint.y - bugPoint.y;
    let angle = Math.atan2(dist_Y,dist_X);

    if(isEscapeMode){
        let degrees = angle * 180/ Math.PI;
        degrees += 180;
        angle = degrees * Math.PI/180;
    }

    return angle;
}

function getVectorBetweenBugAndCursor(mousePoint, bugPoint){
    return new Vector2d(mousePoint.x - bugPoint.x, mousePoint.y - bugPoint.y);
}

function moveBug(mousePoint, bugPoint) {

    let vectorBetweenBugAndCursor = getVectorBetweenBugAndCursor(mousePoint, bugPoint);
    if(vectorBetweenBugAndCursor.getLength() < 5){
        bug.position.x = mousePoint.x;
        bug.position.y = mousePoint.y;
    }

    let normalizedVelocityVector = vectorBetweenBugAndCursor.getNormalizedVector();
    let speedDinamicCoeff = 1;

    
    if(isEscapeMode){
        normalizedVelocityVector = normalizedVelocityVector.getReflectedVector();
    }

    if(isEscapeMode && !isLinearMode){
        speedDinamicCoeff = 1 / vectorBetweenBugAndCursor.getLength() * 200;
    }
    else if(!isEscapeMode && !isLinearMode){
        speedDinamicCoeff = vectorBetweenBugAndCursor.getLength() / 100;
    }

    const newPositionX = bug.position.x + normalizedVelocityVector.point.x * speedDefaultCoeff * speedDinamicCoeff;
    const newPositionY = bug.position.y + normalizedVelocityVector.point.y * speedDefaultCoeff * speedDinamicCoeff;

    if( vectorBetweenBugAndCursor.getLength() > 3 
    && newPositionX <= APP_WIDTH - BUG_INDENT && newPositionX >= BUG_INDENT 
    && newPositionY <= APP_HEIGHT - BUG_INDENT && newPositionY >= BUG_INDENT ){
        bug.position.x = newPositionX;
        bug.position.y = newPositionY;
    }
}

app.ticker.add(function() {

    const mousePoint = new Point2d(app.renderer.plugins.interaction.mouse.global.x, app.renderer.plugins.interaction.mouse.global.y);
    const bugPoint = new Point2d(bug.position.x, bug.position.y);

    bug.rotation = rotateBug(mousePoint, bugPoint);
    moveBug(mousePoint, bugPoint);
});