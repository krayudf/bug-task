import Point2d from "./Point2d";

export default class Vector2d {

    constructor(x, y){
        this.point = new Point2d(x, y);

        this.getLength = () => {
            return Math.sqrt( Math.pow(this.point.x, 2) + Math.pow(this.point.y, 2) );
        }

        this.getNormalizedVector = () => {
            const lengthCurrentVector = this.getLength();
            return new Vector2d(this.point.x / lengthCurrentVector, this.point.y / lengthCurrentVector);
        }

    }

    getReflectedVector = () => {
        return new Vector2d(this.point.x * - 1, this.point.y * - 1);
    }
}